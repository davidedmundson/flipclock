import QtQuick
import org.kde.plasma.plasmoid

WallpaperItem {
    id: root

    width: 1024
    height: 768

    FlipClockView {
        anchors.fill: parent
    }
}
